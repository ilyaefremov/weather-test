import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import * as ReactBootstrap from "react-bootstrap";

import axios from 'axios';


import './index.css';

function formatTime(unixTimestamp){
  var date = new Date(unixTimestamp * 1000);

  return (date.toLocaleString('en', { month: 'long' })) + " " + date.getDate()
}

function getPicture(code){
  code = parseInt(code)

  if (code < 240) return "./thunder.png"
  if (code < 540) return "./rain.png"
  if (code < 650) return "./snow.png"
  if (code < 790) return "./atmosphere.png"
  if (code == 800) return "./clear.png"
  if (code < 850) return "./clouds.png"

  return "./clear.png";

}

class SearchForm extends React.Component {

  render() {

    let buttonText = !this.props.isSearching ? "Search" : null

    return (

      <ReactBootstrap.Col>

        <p className="location-title"> Choose Location </p>


        <ReactBootstrap.InputGroup className="mb-3" >
          <ReactBootstrap.FormControl
            placeholder="City" id="location-input" required
          />
          <ReactBootstrap.InputGroup.Append>

            <ReactBootstrap.Button variant="primary" className="search-button" onClick={() => this.props.getWeatherCast() }> {buttonText}
              <ReactBootstrap.Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
                hidden={!this.props.isSearching}
              />
            </ReactBootstrap.Button>

          </ReactBootstrap.InputGroup.Append>
        </ReactBootstrap.InputGroup>

        <ReactBootstrap.Spinner animation="border" variant="primary" id="spinner"/>

        <div className="game-info">
          <ol>{this.props.places}</ol>
        </div>


      </ReactBootstrap.Col>
    )
  }
}

class CurrentWeather extends React.Component {

  render() {

    let weather = this.props.currentWeather

    if (!this.props.currentWeather) return null

    return (

      <ReactBootstrap.Col>
        <p className="current-weather-title"> Current Weather, {formatTime(weather.dt)} </p>

        <ReactBootstrap.Row className="current-weather-info">

          <ReactBootstrap.Col className="current-weather-main-info">

            <ReactBootstrap.Image src={getPicture(weather.weather[0].id)} className="weather-image"/>

            <p className="weather-image-temperature align-bottom"> {Math.round(weather.temp) + "°C"} </p>

          </ReactBootstrap.Col>

          <ReactBootstrap.Col >

            <WeatherAttributes parameter="Feels like" value={Math.round(weather.feels_like) + "°C"} />
            <WeatherAttributes parameter="Humidity" value={Math.round(weather.humidity) + "%"} />
            <WeatherAttributes parameter="Pressure" value={Math.round(weather.pressure) + " hPa"} />
            <WeatherAttributes parameter="Description" value={weather.weather[0].main} />
            <WeatherAttributes parameter="Wind speed" value={Math.round(weather.wind_speed) + "m/c"} />

          </ReactBootstrap.Col>

        </ReactBootstrap.Row>


      </ReactBootstrap.Col>
    )
  }
}

function WeatherAttributes(props){
    return (

      <ReactBootstrap.Row >

        <ReactBootstrap.Col xs={6}>
          <p className="day-weather-parameter"> {props.parameter}: </p>
        </ReactBootstrap.Col>

        <ReactBootstrap.Col xs={6}>
          <p className="day-weather-parameter-value"> {props.value} </p>
        </ReactBootstrap.Col>

      </ReactBootstrap.Row>

    )
}

class DayForecast extends React.Component {


  render() {

    let weather = this.props.weather

    if (!weather) return null

    return (
      <div>

        <p className="day-weather-title"> {formatTime(weather.dt)} </p>
        <ReactBootstrap.Image src={getPicture(weather.weather[0].id)} className="day-weather-image"/>

        <WeatherAttributes parameter="Temp" value={Math.round(weather.temp.day) + "°C"} />
        <WeatherAttributes parameter="Humidity" value={Math.round(weather.humidity) + "%"} />
        <WeatherAttributes parameter="Pressure" value={Math.round(weather.pressure) + " hPa"} />
        <WeatherAttributes parameter="Wind speed" value={Math.round(weather.wind_speed) + "m/c"} />


      </div>
    )
  
  }

}

class Forecast extends React.Component {

  renderDayForecast(weather) {
    return <DayForecast weather={weather}/>;
  }

  render() {

    let weathers = []

    if (this.props.weekWeather.length == 0) return null

    for (let i = 1; i < 7; i++){
      weathers.push(
        <ReactBootstrap.Col className="day-weather-main-info current-weather-main-info" key={this.props.weekWeather[i].dt}>
          {this.renderDayForecast(this.props.weekWeather[i])}
        </ReactBootstrap.Col>
      )
    }

    return (

      <ReactBootstrap.Row className="justify-content-center">

        {weathers}

      </ReactBootstrap.Row>

    )

  }
}



class WeatherForecast extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      history: [],
      lat: null,
      lng: null,
      current: null,
      week: [],
      isSearching: false,
      isChosen: false
    };
  }

  async componentDidMount () {

    const script = document.createElement("script");

    window.initAutocomplete = this.initAutocomplete.bind(this);

    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCFq58K_fcjd0JZ_71RaSVxORFdjWTHbSw&libraries=places&callback=initAutocomplete";
    script.async = true;
    script.defer = true

    document.body.appendChild(script);
  }

  async initAutocomplete() {
    const input = document.getElementById("location-input")
    const options = {
      fields: ["geometry"]
    }

    var service = new window.google.maps.places.Autocomplete(input);

    service.addListener("place_changed", () => { 
      const place = service.getPlace();

      this.updateHistory({ name: input.value, lat: place.geometry.location.lat().toFixed(2), lng: place.geometry.location.lng().toFixed(2) })

      this.state.lat = place.geometry.location.lat()
      this.state.lng = place.geometry.location.lng()
    })

    document.getElementById("spinner").style.display = "block"
    navigator.geolocation.getCurrentPosition(position => this.successFunction(position), error => this.errorFunction(error)); 
    
    return
  }


  async successFunction(position) {

    this.state.lat = position.coords.latitude
    this.state.lng = position.coords.longitude

    const name = await this.getPlaceName(position.coords.latitude, position.coords.longitude)
    document.getElementById("location-input").value = name

    this.updateHistory({ name: name, lat: position.coords.latitude.toFixed(2) , lng: position.coords.longitude.toFixed(2)  })

    document.getElementById("spinner").style.display = "none"

    await this.getWeatherCast()
  }

  async getPlaceName(lat, lng){

    var geocoder = new window.google.maps.Geocoder();

    return new Promise((resolve, reject) => { 
      geocoder.geocode({ location: { lat: lat, lng: lng }}, (results, status) => {
        if (status === 'OK') {

          for (let addressComponent of results[0].address_components){
            if (addressComponent.types.includes("locality")) return resolve(addressComponent.long_name)
          }

          return resolve(results[0].formatted_address);
        } else {
          reject(status);
        }    
      });
    })

  }

  updateHistory(item){
    let a = this.state.history.filter(function(place) { return place.lat === item.lat })

    if (a.length != 0) return

    this.state.history.push(item)
    localStorage.setItem('history', JSON.stringify(this.state.history));
  }

  errorFunction(error) { 
    if (error) document.getElementById("spinner").style.display = "none"
  }

  deletePlace(lat) {

    let history = this.state.history.concat()

    history = history.filter(function(place) { return place.lat !== lat })

    localStorage.setItem('history', JSON.stringify(history));
    this.setState({ history: history })
  }

  async getWeatherCast() {
    if (!this.state.lat || !this.state.lng) return

    this.setState({isSearching: true})

    const responce = await axios.get("https://api.openweathermap.org/data/2.5/onecall", {params: {lat: this.state.lat, lon: this.state.lng, exclude: "minutely", units: "metric", appid: "0e9c43828643afe6ea16765cfde0d4c6"}})

    this.setState({isSearching: false, current: responce.data.current, week: responce.data.daily})
  }

  async searchHistoryItem(place) {
    this.state.lat = place.lat
    this.state.lng = place.lng

    const input = document.getElementById("location-input")
    input.value = place.name

    await this.getWeatherCast()
  }

  render() {

    this.state.history = localStorage.getItem('history') ? JSON.parse(localStorage.getItem('history')) : []

    let places = []

    for (let place of this.state.history){
      places.push(
        <li key={place} className="history-item">
          <p className="history-item-name" onClick={() => this.searchHistoryItem(place) } > {place.name} </p>
          <img src="./delete.png" className="cross" onClick={() => this.deletePlace(place.lat)} />
        </li>
      )
    }

    return (

      <ReactBootstrap.Container className="">

        <ReactBootstrap.Col xs={{span: 10, offset: 1}}>

          <div className="main">

            <div className="top">
              <ReactBootstrap.Row>

                <SearchForm places={places} isSearching={this.state.isSearching} isChosen={this.state.isChosen} getWeatherCast={(i) => this.getWeatherCast()}/>
                <CurrentWeather currentWeather={this.state.current}/>

              </ReactBootstrap.Row>
            </div>

            <div className="bottom">

              <Forecast weekWeather={this.state.week}/>

            </div>

          </div>

        </ReactBootstrap.Col>

      </ReactBootstrap.Container>
    );
  }
}


// ========================================

ReactDOM.render(
  <WeatherForecast />,
  document.getElementById('root')
);

